import { initializeApp } from 'firebase'
import { config } from './key-firebase'

const app = initializeApp(config)

export const db = app.database()
export const namesRef = db.ref('results')