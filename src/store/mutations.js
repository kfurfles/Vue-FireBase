const mutations = {
    SET_USER (store, value){
        store.user = value
    },
    SET_API_SUCCESS (store, value){
        store.data = value
    }
}

export { mutations }